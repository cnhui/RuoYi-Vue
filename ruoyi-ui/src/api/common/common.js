import request from '@/utils/request'

// 删除文件
export function deleteFile(fileId) {
  return request({
    url: '/common/deleteFile/' + fileId,
    method: 'get'
  })
}// 下载文件
export function download(fileId) {
  return request({
    url: '/common/download/' + fileId,
    method: 'get'
  })
}
