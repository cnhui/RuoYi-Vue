package com.ruoyi.moxi.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.moxi.domain.MoxiFile;
import com.ruoyi.moxi.service.IMoxiFileService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 文件Controller
 * 
 * @author ruoyi
 * @date 2023-02-02
 */
@RestController
@RequestMapping("/moxi/file")
public class MoxiFileController extends BaseController
{
    @Autowired
    private IMoxiFileService moxiFileService;

    /**
     * 查询文件列表
     */
    @PreAuthorize("@ss.hasPermi('moxi:file:list')")
    @GetMapping("/list")
    public TableDataInfo list(MoxiFile moxiFile)
    {
        startPage();
        List<MoxiFile> list = moxiFileService.selectMoxiFileList(moxiFile);
        return getDataTable(list);
    }

    /**
     * 导出文件列表
     */
    @PreAuthorize("@ss.hasPermi('moxi:file:export')")
    @Log(title = "文件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MoxiFile moxiFile)
    {
        List<MoxiFile> list = moxiFileService.selectMoxiFileList(moxiFile);
        ExcelUtil<MoxiFile> util = new ExcelUtil<MoxiFile>(MoxiFile.class);
        util.exportExcel(response, list, "文件数据");
    }

    /**
     * 获取文件详细信息
     */
    @PreAuthorize("@ss.hasPermi('moxi:file:query')")
    @GetMapping(value = "/{fileId}")
    public AjaxResult getInfo(@PathVariable("fileId") Long fileId)
    {
        return success(moxiFileService.selectMoxiFileByFileId(fileId));
    }

    /**
     * 新增文件
     */
    @PreAuthorize("@ss.hasPermi('moxi:file:add')")
    @Log(title = "文件", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MoxiFile moxiFile)
    {
        return toAjax(moxiFileService.insertMoxiFile(moxiFile));
    }

    /**
     * 修改文件
     */
    @PreAuthorize("@ss.hasPermi('moxi:file:edit')")
    @Log(title = "文件", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MoxiFile moxiFile)
    {
        return toAjax(moxiFileService.updateMoxiFile(moxiFile));
    }

    /**
     * 删除文件
     */
    @PreAuthorize("@ss.hasPermi('moxi:file:remove')")
    @Log(title = "文件", businessType = BusinessType.DELETE)
	@DeleteMapping("/{fileIds}")
    public AjaxResult remove(@PathVariable Long[] fileIds)
    {
        return toAjax(moxiFileService.deleteMoxiFileByFileIds(fileIds));
    }
}
