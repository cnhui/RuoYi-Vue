package com.ruoyi.moxi.mapper;

import java.util.List;
import com.ruoyi.moxi.domain.MoxiFile;

/**
 * 文件Mapper接口
 * 
 * @author ruoyi
 * @date 2023-02-02
 */
public interface MoxiFileMapper 
{
    /**
     * 查询文件
     * 
     * @param fileId 文件主键
     * @return 文件
     */
    public MoxiFile selectMoxiFileByFileId(Long fileId);

    /**
     * 查询文件列表
     * 
     * @param moxiFile 文件
     * @return 文件集合
     */
    public List<MoxiFile> selectMoxiFileList(MoxiFile moxiFile);

    /**
     * 新增文件
     * 
     * @param moxiFile 文件
     * @return 结果
     */
    public int insertMoxiFile(MoxiFile moxiFile);

    /**
     * 修改文件
     * 
     * @param moxiFile 文件
     * @return 结果
     */
    public int updateMoxiFile(MoxiFile moxiFile);

    /**
     * 删除文件
     * 
     * @param fileId 文件主键
     * @return 结果
     */
    public int deleteMoxiFileByFileId(Long fileId);

    /**
     * 批量删除文件
     * 
     * @param fileIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMoxiFileByFileIds(Long[] fileIds);
}
