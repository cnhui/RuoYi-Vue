package com.ruoyi.moxi.service.impl;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.moxi.mapper.MoxiFileMapper;
import com.ruoyi.moxi.domain.MoxiFile;
import com.ruoyi.moxi.service.IMoxiFileService;

/**
 * 文件Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-02-02
 */
@Service
public class MoxiFileServiceImpl implements IMoxiFileService 
{
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MoxiFileMapper moxiFileMapper;

    /**
     * 查询文件
     * 
     * @param fileId 文件主键
     * @return 文件
     */
    @Override
    public MoxiFile selectMoxiFileByFileId(Long fileId)
    {
        return moxiFileMapper.selectMoxiFileByFileId(fileId);
    }

    /**
     * 查询文件列表
     * 
     * @param moxiFile 文件
     * @return 文件
     */
    @Override
    public List<MoxiFile> selectMoxiFileList(MoxiFile moxiFile)
    {
        return moxiFileMapper.selectMoxiFileList(moxiFile);
    }

    /**
     * 新增文件
     * 
     * @param moxiFile 文件
     * @return 结果
     */
    @Override
    public int insertMoxiFile(MoxiFile moxiFile)
    {
        moxiFile.setCreateTime(DateUtils.getNowDate());
        return moxiFileMapper.insertMoxiFile(moxiFile);
    }

    /**
     * 修改文件
     * 
     * @param moxiFile 文件
     * @return 结果
     */
    @Override
    public int updateMoxiFile(MoxiFile moxiFile)
    {
        moxiFile.setUpdateTime(DateUtils.getNowDate());
        return moxiFileMapper.updateMoxiFile(moxiFile);
    }

    /**
     * 批量删除文件
     * 
     * @param fileIds 需要删除的文件主键
     * @return 结果
     */
    @Override
    public int deleteMoxiFileByFileIds(Long[] fileIds)
    {

        Arrays.asList(fileIds).stream().forEach((fileId)->{
            MoxiFile moxiFile = moxiFileMapper.selectMoxiFileByFileId(fileId);
            File file = new File(moxiFile.getAbsolutePath());
            if(file.exists()){
                file.delete();
                logger.debug("文件删除 id:{}, 路径: {}",fileId, file.getAbsolutePath());
            }
        });

        return moxiFileMapper.deleteMoxiFileByFileIds(fileIds);
    }

    /**
     * 删除文件信息
     * 
     * @param fileId 文件主键
     * @return 结果
     */
    @Override
    public int deleteMoxiFileByFileId(Long fileId)
    {
        MoxiFile moxiFile = moxiFileMapper.selectMoxiFileByFileId(fileId);
        File file = new File(moxiFile.getAbsolutePath());
        if(file.exists()){
            file.delete();
            logger.debug("文件删除 id:{}, 路径: {}", fileId, file.getAbsolutePath());
        }
        return moxiFileMapper.deleteMoxiFileByFileId(fileId);
    }
}
