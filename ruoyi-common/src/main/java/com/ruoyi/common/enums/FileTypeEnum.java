package com.ruoyi.common.enums;

/**
 * 用户状态
 * 
 * @author ruoyi
 */
public enum FileTypeEnum
{
    word("1", "正常"),
    excel("2", "停用"),
    pdf("3", "删除"),
    txt("4", "删除"),
    pic("5", "图片"),
    video("6", "视频"),
    audio("7", "音频"),
    other("8", "其他");

    private final String code;
    private final String info;

    FileTypeEnum(String code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public String getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
